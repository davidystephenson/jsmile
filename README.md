# JSMiLe
### J:)

A JavaScript Markup Language that plays nice.

## Installation
### Node
`npm install jsmile`

### Browser
A fully bundled and transformed implementation, ready for use in browsers including any version of Internet Explorer or Opera Mini, is provided in the `dist/jsmile.js` file. 
Including this script will expose the entire package as a global object named `jsmile`.
```
<script src="./node_modules/jsmile/dist/jsmile.js"></script>
```

## Usage
### Build
The `build` method outputs an HTML string from JavaScript data.
It is an alias for the [`jsml-davidystephenson`](https://bitbucket.org/davidystephenson/jsml-davidystephenson#readme) package.
```
jsmile.build('hello world')
// 'hello world'

jsmile.build({})
// '<div></div>'

const jsmile.build({ child: 'hello world' })
// '<div>hello world</div>'

jsmile.build({ tag: 'h1' child: 'Welcome!' })
// '<h1>Welcome!</h1>'

jsmile.build({ tag: 'img', src: 'imgur.com' })
// '<img src='imgur.com'>'

const format = name => [
  { tag: 'h3', class: 'title', child: 'User page' }, 
  { tag: 'p', child: `Welcome ${name}.`}
]
jsmile.build(format('Someone'))
// '<h3 class="title">User page</h3><p>Welcome Someone.</p>'
```

### Browser
The `browser` method renders JavaScript DOM element from JavaScript data.
It is an alias for the [`jsmile-browser`](https://bitbucket.org/davidystephenson/jsmile-browser#readme) package.
```
const render = jsmile.browser(window)

const div = render({})
console.log(div.tagName)
// DIV
```

In the browser, or anywhere else where a DOM window with a valid document is globally available, a `jsmile.browser` instance will be created and attached as `jsmile.render`.
A shortcut for this function will also be added globally as `jsml`.
```
<script src="./node_modules/jsmile/dist/jsmile.js"></script>
<script>
  console.log(jsmile)
  // { build: f, express: f, browser: f, render: f }

  const span = jsmile.render({ tag: 'span', child: 'hello world' })
  console.log(span.outerHTML)
  // '<span>hello world</span>'

  const div = jsml({})
  console.log(div.outerHTML)
  // '<div></div>'
</script>
```

### Express
#### Initialization
The `express` method integrates `jsmile` with [`express`]() as a view engine.
It is an alias for the [`jsmile-express`]() package.

After setting a views directory, declare a view engine that will read `.js` files in the directory.
Then, initialize the engine by passing the express application itself to jsmile's `express` method.
```
const express = require('express')
const jsmile = require('jsmile')

const app = express()

app.set('views', './views')
app.set('view engine', 'js')
app.engine('js', jsmile.express(app))
```

#### Rendering
You can then use the `express.render` method to render view files by name, optionally passing any data the view uses.
View files are JavaScript modules that export a function.

The function is called when the view is rendered by your routing logic.
The output is parsed by the `jsmile.build` method and then sent as the request's response.
The function is passed two arguments:
1. The jsmile library.
1. An options object containing the data passed to the route.

`views/index.js`
```
module.exports = (options, jsmile) => {
  const title = options.message.toUpperCase()

  return {
    tag: 'html',
    child: [
      { tag: 'head' },
      { 
        tag: 'body'
        child: {
          tag: 'h1',
          child: title 
        }
      }
    ]
  }
}
```

```
app.get('/', (request, response) => response.render('index', { message: 'Hello user!' }))
// <html><head></head><body><h1>HELLO USER!</h2></body></html>
```

#### Including

The `jsmile` library passed to views has an additional method, `include`.

The include method allows other views to be incorporated.
It works similarly to the Express `render` method.
It takes two arguments:
1. The name of the view.
2. An optional object with data used in the function.

`views/navbar.js`
```
module.exports = () => ({
  class: 'navbar',
  child: [
    {
      tag: 'a',
      href: '/',
      child: 'Home'
    }
  ]
})
```

`views/layout.js`
```
module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    { tag: 'head', child: options.head },
    {
      tag: 'body',
      child: [
        jsmile.include('navbar'),
        options.body
      ]
    }
  ]
})
```

`views/title.js`
```
module.exports = (jsmile, options) => ({
  class: 'title',
  tag: 'h1',
  child: options.text
})
```

`views/index.js`
```
module.exports = (jsmile, options) => jsmile.include('layout', {
  body: [
    jsmile.include('title', { text: 'Welcome'}),
    { tag: 'p', child: options.message }
  ]
})
```

```
app.get('/', (request, response) => response.render('index', { message: 'Hello World!' }))
///<html><head></head><body><div class="navbar"><a href="/">Home</a></div><h1 class="title">Welcome</h1><p>Hello World!</p></body></html>
```
