module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    options.head
      ? { tag: 'head', child: options.head }
      : null,
    {
      tag: 'body',
      child: [
        jsmile.include('navbar'),
        options.body
      ]
    }
  ]
})
