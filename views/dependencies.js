module.exports = (jsmile, options) => [
  { child: 'hello' },
  jsmile.dependency('fake', { child: 'world' }),
  options.depend
    ? jsmile.depend('fake')
    : null
]
