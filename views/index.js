module.exports = (jsmile, options) => jsmile.include('layout', {
  body: [
    jsmile.include('title', { text: 'Welcome' }),
    { tag: 'p', child: options.message }
  ]
})
