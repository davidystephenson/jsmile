const jsmileBrowser = require('jsmile-browser')

const library = {
  build: require('jsml-davidystephenson'),
  express: require('jsmile-express'),
  renderer: jsmileBrowser
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  library.render = library.renderer(window)
  window.jsml = library.render
}

module.exports = library
